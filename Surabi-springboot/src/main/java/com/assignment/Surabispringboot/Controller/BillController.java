package com.assignment.Surabispringboot.Controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.Surabispringboot.Model.Bill;
import com.assignment.Surabispringboot.Service.BillService;

@CrossOrigin(origins = {"http://localhost:3000","http://localhost:4200"})
@RestController
public class BillController {


	@Autowired
	BillService billservice;

	//route for user to generate bill for the items he has added to his cart
	@GetMapping("user/{userid}/generatebill")
	public String billGenerate(@PathVariable(value = "userid") Integer userId)
	{
		return billservice.billGenerate(userId);
	}



	//route for admin to view bills getting generated the current day
	@GetMapping("/admin/{adminid}/getbill/today")
	public List<Bill> gettodayBill(@PathVariable(value = "adminid") Integer adminid)
	{
		return billservice.gettodayBill();
	}



	//route for admin to view the total sale 
	//admin has to specify month value in th below URL in integer
	//01 corresponds to january
	//02 corresponds to february and so on
	//03....04.....05 for corrresponding months
	@GetMapping("/admin/{adminid}/getsale/{month}")
	public String getTotalSale(@PathVariable(value = "adminid") Integer adminid,@PathVariable(value = "month") Integer monthid)
	{
		return billservice.getTotalSale(monthid);
	}



	//route for admin to view the total sale along with the customers and their corresponding bill amount in the given month
	//admin has to specify month value in the below URL in integer
	//01 corresponds to january
	//02 corresponds to february and so on
	@GetMapping("/admin/{adminid}/getsale/{month}/view")
	public CombineObject getSaleView(@PathVariable(value = "adminid") Integer adminid,@PathVariable(value = "month") Integer monthid)
	{
		return billservice.getSaleView(monthid);
	}

}