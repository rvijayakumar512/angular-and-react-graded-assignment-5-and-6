package com.assignment.Surabispringboot.Controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.Surabispringboot.Model.Item;
import com.assignment.Surabispringboot.Service.ItemService;

@CrossOrigin(origins = {"http://localhost:3000","http://localhost:4200"})
@RestController
public class ItemController {
	public List<Item> glist=new ArrayList<>();
	public double bill=0;

	@Autowired
	ItemService itemservice;

	//route for user to display veg as per user preference
	@GetMapping("/user/{userid}/display/veg")
	public List<Item> displayVeg(@PathVariable(value = "userid") Integer UserId) {
		return itemservice.displayVeg();
	}

	//route for user to display nonveg as per user preference
	@GetMapping("/user/{userid}/display/nonveg")
	public List<Item> displayNonveg(@PathVariable(value = "userid") Integer UserId) {
		return itemservice.displayNonVeg();
	}

	//route for user to display softdrinks as per user preference
	@GetMapping("/user/{userid}/display/softdrinks")
	public List<Item> displaySoftdrinks(@PathVariable(value = "userid") Integer UserId) {
		return itemservice.displaySoftdrinks();
	}

	//route for user to display dessert as per user preference
	@GetMapping("/user/{userid}/display/dessert")
	public List<Item> displayDessert(@PathVariable(value = "userid") Integer UserId) {
		return itemservice.displayDessert();
	}

	//route for user to add a item to cart by specfying their sequence number
	@PostMapping("/user/{userid}/additem")
	public String addItem(@RequestBody List<Item> item) throws Exception {
		return itemservice.addItem(item);
	}

	//route for user to view cart ..to view the items he has added
	@GetMapping("/user/{userid}/showcart")
	public List<Item> showcart(@PathVariable(value = "userid") Integer UserId) throws Exception {
		return itemservice.showCart();
	}

}
