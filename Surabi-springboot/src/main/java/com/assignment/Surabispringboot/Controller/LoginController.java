package com.assignment.Surabispringboot.Controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.assignment.Surabispringboot.Model.User;
import com.assignment.Surabispringboot.Service.UserService;

@CrossOrigin(origins = {"http://localhost:3000","http://localhost:4200"})
@RestController
public class LoginController {

	@Autowired
	UserService userservice;
	
	
	//route to register a user/admin
	@PostMapping("/user/register")
	public String Register(@RequestBody User user) throws Exception {
		return userservice.register(user);
	}
	
	
	//route for user to login
	@PostMapping("/user/login")
	public String userLogin(@RequestBody User user) throws Exception {
		return userservice.userLogin(user);
	}

	//route for admin to login
	@PostMapping("/admin/login")
	public String adminLogin(@RequestBody User admin) {
		return userservice.adminLogin(admin);
	}

	//route for admin to view user by specifying the user id
	@GetMapping("/admin/viewuser/{userid}")
	public User viewUser(@PathVariable(value = "userid") Integer userId)  {
		return userservice.viewUser(userId);
	}

	//route for admin to view all users
	@GetMapping("/admin/viewusers")
	public List<User> viewAllUser()  {
		return userservice.viewAllUser();
	}

	//route for admin to add users
	@PostMapping("/admin/adduser/{userid}")
	public String addUser(@PathVariable(value = "userid") Integer userId,@RequestBody User userDetails)  {
		return userservice.addUser(userId,userDetails);
	}

	//route for admin to update a user by their userid
	@PutMapping("/admin/updateuser/{userid}")
	public String updateUser(@PathVariable(value = "userid") Integer userId,@RequestBody User userDetails)  {
		return userservice.updateUser(userId,userDetails);
	}

	//route for admin to delete a user by their userid
	@DeleteMapping("admin/deleteuser/{userid}")
	public String deleteUser(@PathVariable(value = "userid") Integer userId) {
		return userservice.deleteUser(userId);
	}

	//route for user to logout
	@GetMapping("user/{userid}/logout")
	public String LogoutUser(@PathVariable(value = "userid") Integer userId)
	{
		return userservice.logoutuser(userId);
	}

	//route for admin to logout
	@GetMapping("admin/{adminid}/logout")
	public String LogoutAdmin(@PathVariable(value = "adminid") Integer adminId)
	{ 
		return userservice.logoutadmin(adminId);

	}
}


