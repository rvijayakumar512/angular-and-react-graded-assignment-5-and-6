package com.assignment.Surabispringboot.Service;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.Surabispringboot.Controller.CombineObject;
import com.assignment.Surabispringboot.Controller.ItemController;
import com.assignment.Surabispringboot.Model.Bill;
import com.assignment.Surabispringboot.Model.Item;
import com.assignment.Surabispringboot.Model.User;
import com.assignment.Surabispringboot.Repository.BillRepository;
import com.assignment.Surabispringboot.Repository.LoginRepository;

@Service
public class BillService {
	@Autowired
	ItemController itemcontroller;

	@Autowired
	BillRepository billRepo;

	@Autowired
	LoginRepository loginRepo;

	//implementation of generating bill by user
	public String billGenerate(Integer userId)
	{
		int i=0;
		Iterator<Item> it=itemcontroller.glist.iterator();
		while(it.hasNext())
		{
			Item item=itemcontroller.glist.get(i);
			itemcontroller.bill+=Double.parseDouble(item.getCost());
			i++;
			it.next();
		}
		Bill b=billRepo.getBillId();
		int id=b.getId()+1;
		int userid=userId.intValue();
		User user=loginRepo.getUser(userid);
		String name=user.getUsername();
		String amount=Double.toString(itemcontroller.bill);
		LocalDate today = LocalDate.now();
		String year  = Integer.toString(today.getYear());
		String month = Integer.toString(today.getMonthValue());
		String day   = Integer.toString(today.getDayOfMonth());
		//itemcontroller.glist.clear();
		billRepo.insertdb(id,name,amount,day,month,year);
		return "Your bill is "+ itemcontroller.bill;
	}


	//implementation of generating current date bill for admin
	public List<Bill> gettodayBill()
	{
		LocalDate today = LocalDate.now();
		String year  = Integer.toString(today.getYear());
		String month = Integer.toString(today.getMonthValue());
		String day   = Integer.toString(today.getDayOfMonth());	
		List<Bill> llist=billRepo.getTodayBill(day,month,year);
		return llist;
	}


	//implementation of total sale of a month for admin
	public String getTotalSale(Integer monthid)
	{
		String month=Integer.toString(monthid);
		List<Bill> blist=billRepo.getTotalSale(month);
		Iterator it=blist.iterator();
		double sale=0;
		while(it.hasNext())
		{
			Bill b=(Bill) it.next();
			String totalsale=b.getAmount();
			sale+=Double.parseDouble(totalsale);
		}
		return "Your total sale for this month is...."+Double.toString(sale);
	}




	//implementation of getsale view along with total sale and users contributing to the total sale of that month (admin)
	public CombineObject getSaleView(Integer monthid)
	{
		String month=Integer.toString(monthid);
		List<Bill> blist=billRepo.getTotalSale(month);
		Iterator it=blist.iterator();
		double sale=0;
		while(it.hasNext())
		{
			Bill b=(Bill) it.next();
			String totalsale=b.getAmount();
			sale+=Double.parseDouble(totalsale);
		}
		CombineObject ob=new CombineObject(sale,blist);
		return ob;
	}
}
