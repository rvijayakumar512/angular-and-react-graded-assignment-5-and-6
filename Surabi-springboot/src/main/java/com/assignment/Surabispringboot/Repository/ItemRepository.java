package com.assignment.Surabispringboot.Repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.Surabispringboot.Model.Item;
//extends JPA repository
public interface ItemRepository extends JpaRepository<Item, Integer>{
	//specifying methods of named native query
	List<Item> display(String classification);
	Item add(int itemno,String itemname,String itemclassification);
	Item checkdb(int itemno);
}
