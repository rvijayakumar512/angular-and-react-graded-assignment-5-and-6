# Angular and React Assignment

## Folder Information

* Surabi-springboot => This is the backend code done using springboot. Run this in eclipse IDE.
* user-app => This is the front UI designed for users functionality in React.
* admin-app => This is the front UI designed for admin functionality in Angular.
* surabi.sql => This is the script file. Use this to configure and initialise the databse in MYSQL.

* Run the surabi.sql file in mysql command line and set up the database 
* After opening the Surabi-springboot folder in Eclipse in your local machine, replace the exisiting database username and password with your own username and password for the database in **application.properties** file


#### Separate Readme.md files are given inside admin-app and user-app folders for testing functionality
