import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerformcrudComponent } from './performcrud.component';

describe('PerformcrudComponent', () => {
  let component: PerformcrudComponent;
  let fixture: ComponentFixture<PerformcrudComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerformcrudComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformcrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
