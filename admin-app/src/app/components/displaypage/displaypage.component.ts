import { Component, OnInit } from '@angular/core';
import {BillserviceService} from '../../service/billservice.service'
import { RouterModule, Routes,Router } from '@angular/router';
import {Bill} from  '../../model/bill';
import {Totalbill} from  '../../model/totalbill';
@Component({
  selector: 'app-displaypage',
  templateUrl: './displaypage.component.html',
  styleUrls: ['./displaypage.component.css']
})
export class DisplaypageComponent implements OnInit {

bills:Bill [] =new Array<Bill>();
bill:Bill =new Bill();
show:boolean =false;
sales:boolean=false;
totalbill:Totalbill=new Totalbill();
  constructor(private service:BillserviceService, private route:Router) { }

  ngOnInit(): void {

  }
viewbills(){
  console.log("in bills");
this.service.fetchTodayBills().subscribe(
  data=>{
    this.sales=false;
    this.show=true;
    console.log(data);
    this.bills=new Array<Bill>();
    this.bills=data;
  },
  error=>{
    console.log(error.response);
  }
)
}
getsale(){

  this.service.getTotalSales(this.totalbill.month).subscribe(
    data=>{
      this.show=false;
      this.sales=true;
      this.totalbill.totalbill=data.totalbill;
      this.bills=new Array<Bill>();
      this.bills=data.list;
      console.log(this.totalbill.month);

    },
    error=>{
      console.log(error.response);

    }
  )
}
performcrud(){
  this.route.navigate(['crud']);
}

}
