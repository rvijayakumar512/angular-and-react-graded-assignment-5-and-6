import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LogincontainerComponent} from './container/logincontainer/logincontainer.component';
import {ViewcontainerComponent} from './container/viewcontainer/viewcontainer.component';
import {CrudcontainerComponent} from './container/crudcontainer/crudcontainer.component';
const routes: Routes = [
  {path:'', component:LogincontainerComponent},
  {path:'view', component:ViewcontainerComponent},
  {path:'crud', component:CrudcontainerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
