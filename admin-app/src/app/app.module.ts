import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginformComponent } from './components/loginform/loginform.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import { LogincontainerComponent } from './container/logincontainer/logincontainer.component';
import { ViewcontainerComponent } from './container/viewcontainer/viewcontainer.component';
import { DisplaypageComponent } from './components/displaypage/displaypage.component';
import { CrudcontainerComponent } from './container/crudcontainer/crudcontainer.component';
import { PerformcrudComponent } from './components/performcrud/performcrud.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginformComponent,
    LogincontainerComponent,
    ViewcontainerComponent,
    DisplaypageComponent,
    CrudcontainerComponent,
    PerformcrudComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
      MatToolbarModule,
      MatInputModule,
      MatButtonModule,
      FormsModule,
        HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
