import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';
@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

  constructor(private httpclient:HttpClient) { }

  fetchUsers():Observable<any>{
  return  this.httpclient.get<any>("http://localhost:8083/admin/viewusers");
  }
  addUsers(user:User):Observable<any>{
    console.log(user.userid);
    user.userrole="user";
  return  this.httpclient.post<any>("http://localhost:8083/admin/adduser/"+user.userid,user);
  }

  updateUsers(user:User):Observable<any>{
    console.log(user.userid);
    user.userrole="user";
  return  this.httpclient.put<any>("http://localhost:8083/admin/updateuser/"+user.userid,user);
  }

  deleteUsers(user:User):Observable<any>{
    console.log(user.userid);
    user.userrole="user";
  return  this.httpclient.delete<any>("http://localhost:8083/admin/deleteuser/"+user.userid);
  }
}
