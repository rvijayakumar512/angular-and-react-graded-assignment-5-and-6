import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BillserviceService {

  constructor(private httpclient:HttpClient) { }

  fetchTodayBills():Observable<any>{
    const id=localStorage.getItem('id');
  return  this.httpclient.get<any>("http://localhost:8083/admin/"+id+"/getbill/today");
  }

  getTotalSales(month:string):Observable<any>{
    const id=localStorage.getItem('id');
  return  this.httpclient.get<any>("http://localhost:8083/admin/"+id+"/getsale/"+month+"/view");
  }
}
