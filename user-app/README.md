
# Angular and React Assignment

## User Panel - React

### Setting Up Environment
* Clone the git repo.
* user-app folder contains all the functionalities of users implemented in React JS
* move into the the user-app folder after cloning.
* Run **npm install** command to install all the required node modules into your local system. 
* Run **npm start** to initialise React development server.
* Once the server is set up, now in the browser type **http://localhost:3000/**

### Testing User functionalities

#### Registration
* User has registration functionality. If new user then click on register in register screen.
* Set your username and password as part of registration process.
* If the credentials you have entered is already present in databse you will be alerted with you cannot perform resistration. In that case try another username and password.
* Post successful regisatration you will be redirected to login page , now enter the credentials you registered with to login as user.
#### Login
* Enter your username and password and click login button.
* If you are a valid user (credentials matches in db) you will be taken to home page
* If you are not a valid user , nothing will happen, you can try registering and then try again.
* An example of user credential is given below, if you want you can use it to login
   * username: kumar
   * password: kumar123
#### Home page to Display items
* Click display veg, display non veg, display soft drink or display dessert to view all the items available in the restaurant in that particular category.
* On clicking any of those above you will view all items of that category.
* If you wish to order that item..click on add to cart button on the right column of that particular item.
* Add items to cart as per your wish.
* Once you click on add to cart that items will be added to cart.
* Now you can view cart by clicking on View cart button on top right of the screen.

#### View Cart
* On clicking view cart button you will be redirected to a new page where you will view all the items you added to cart.
* If you wish to add some more items click on back button in view cart page and go back to the previous display page and add few more items.

#### Placing Order
* Now if you have added all the neede items in the view cart page click on generate bill button to view your bill for your items.
* Your bill amount will pop up in the screen.
* Click exit button in pop up.

#### Placing another order for same logged in user
* After placing an order ..a button for placing another order will be enabled.
* If you want to place another order click on that button and follow the same steps to place order as I have mentioned above.

#### Logout
* If the user wishes to Logout enter logout button in view cart screen.
* A alert will appear and then ypu will be redirected to login page again now.