import React, {Component, useState,useEffect} from 'react';
import { Form, InputGroup, FormGroup, FormControl, ControlLabel,Button} from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Link,
  useHistory,
} from 'react-router-dom';




function Register(){

  const [uname,setuname]=useState('');
  const [upass,setupass]=useState('');
  const [redir,setredir]=useState(false);
  const handlechange=(e)=>{
    e.preventDefault();
    const name=e.target.name;
    if(name=="username")
    {
    setuname(e.target.value);
    }
    else{
    setupass(e.target.value);
  }
  }

const handlesubmit=()=>{
  var flag=0;
  const data= {
          username: uname,
          userpassword: upass,
      }
      fetch("http://localhost:8083/admin/viewusers").
      then(response=>response.json()).
      then(data=>{
        console.log(data);
        data.forEach((data) => {
          if(data.username==uname && data.userpassword==upass)
          {
            flag=1;
            setredir(false);
            alert("User with same credentials already exists!!\nCannot Register as new user\nTry Login to application");
          }
        });

      });
if(flag==0){
  const result=fetch("http://localhost:8083/user/register", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
   setredir(true);
  }
}

  return(
    <div>
    <Link to="/" rel="noopener noreferrer" className="btn btn-primary" style={{marginLeft:'1100px'}} >Back to Login</Link>
    <div class="container center_div" style={{width:'45%'}}>
    <h1>Welcome to Registration Page</h1><br/><br/><br/>
  <Form.Group className="mb-3" controlId="formBasicEmail" >
    <Form.Label>Enter UserName</Form.Label>
    <Form.Control type="text" name="username" value={uname} onChange={handlechange} placeholder="Enter Username" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Enter Password</Form.Label>
    <Form.Control type="password" name="userpass" value={upass} onChange={handlechange} placeholder="Enter Password" />
  </Form.Group>
  <div style={{paddingLeft:'270px'}}>
  <Button variant="primary" type="submit" onClick={handlesubmit}>
    Register
  </Button>
  {redir&&<Redirect to='/'/>}
  </div>
</div>
</div>
  );
}

export default Register;
